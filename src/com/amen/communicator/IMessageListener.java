package com.amen.communicator;

public interface IMessageListener {
	void messageReceived(String msg);
}
