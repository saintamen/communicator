package com.amen.communicator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerRunnable implements Runnable {

	private BufferedReader reader;
	private PrintWriter writer;
	private IMessageListener listener;
	public ServerRunnable(IMessageListener listener) {
		super();
		this.listener = listener;
	}

	@Override
	public void run() {
		try {
			// otwieramy port
			ServerSocket socketPolaczenia = new ServerSocket(10001);
			while (true) { // nieskonczona petla
				// przyjęcie połączenia
				Socket otwartySocket = socketPolaczenia.accept();

				// otwieranie wyjscia
				OutputStream kanalWyjscia = otwartySocket.getOutputStream();
				writer = new PrintWriter(kanalWyjscia);

				// otwieranie wejscia
				InputStream kanalWejscia = otwartySocket.getInputStream();
				reader = new BufferedReader(new InputStreamReader(kanalWejscia));
				
				String otrzymanaLinia = null;
				do {
					otrzymanaLinia = reader.readLine();
					listener.messageReceived(otrzymanaLinia);
				} while (otrzymanaLinia != null);
				
				//
				reader.close();
				writer.close();
			}
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
			ioe.printStackTrace();
		}
	}
	
	public void sendMessage(String tresc){
		writer.println(tresc);
		writer.flush();
	}
}